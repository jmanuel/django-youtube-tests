from django.test import TestCase, Client

from . import views
from . import models

class YoutubeTestCase(TestCase):
    def test_get_ok(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_post_ok(self):
        client = Client()
        response = client.post('/', {'action': 'Select', 'title': 'XML Práctico: Chistes'})
        self.assertEqual(response.status_code, 200)

    def test_main_ok(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.resolver_match.func, views.main)
